#![no_std]
extern crate alloc;

mod hex_string;

use alloc::vec::Vec;
use serde::Deserialize;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SystemData {
    pub has_encrypted_images: bool,
    pub has_encrypted_audio: bool,
    #[serde(with = "hex_string")]
    pub encryption_key: Vec<u8>,
}

pub fn decrypt(key: &[u8], buf: &mut [u8]) {
    // RPG Maker MV uses basic xor encryption.
    key.iter().zip(buf.iter_mut()).for_each(|(k, b)| *b ^= k);
}

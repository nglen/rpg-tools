use core::fmt;
use core::iter::FromIterator;
use core::marker::PhantomData;
use serde::de::{self, Deserializer, Visitor};

pub fn deserialize<'de, T, D>(des: D) -> Result<T, D::Error>
where
    T: FromIterator<u8>,
    D: Deserializer<'de>,
{
    let visitor = HexStringVisitor(PhantomData);
    des.deserialize_str(visitor)
}

// split into sets of two characters and interpret each as a byte in hex
struct HexStringVisitor<T>(PhantomData<fn() -> T>);
impl<'de, T> Visitor<'de> for HexStringVisitor<T>
where
    T: FromIterator<u8>,
{
    type Value = T;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("an even length hex string")
    }

    fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        let hex: &[u8] = s.as_ref();

        if hex.len() % 2 != 0 {
            Err(de::Error::custom("odd length hex string"))?;
        }

        hex.chunks_exact(2)
            .map(|v| hex_pair(v[0], v[1]).ok_or(de::Error::custom("invalid hex digit")))
            .collect()
    }
}

fn hex_digit(v: u8) -> Option<u8> {
    match v {
        b'0' => Some(0),
        b'1' => Some(1),
        b'2' => Some(2),
        b'3' => Some(3),
        b'4' => Some(4),
        b'5' => Some(5),
        b'6' => Some(6),
        b'7' => Some(7),
        b'8' => Some(8),
        b'9' => Some(9),
        b'a' | b'A' => Some(10),
        b'b' | b'B' => Some(11),
        b'c' | b'C' => Some(12),
        b'd' | b'D' => Some(13),
        b'e' | b'E' => Some(14),
        b'f' | b'F' => Some(15),
        _ => None,
    }
}

fn hex_pair(high: u8, low: u8) -> Option<u8> {
    let high = hex_digit(high)?;
    let low = hex_digit(low)?;

    Some(high * 16 + low)
}

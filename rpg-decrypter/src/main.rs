//! Decrypt RPG Maker MV Resources
//!
//! Common Extensions:
//!  ogg -> rpgmvo
//!  m4a -> rpgmvm
//!  png -> rpgmvp
use argh::FromArgs;
use rpg_mv::SystemData;
use std::ffi::OsStr;
use std::fs::File;
use std::io::{self, BufReader, BufWriter, Read, Write};
use std::path::{Path, PathBuf};
use walkdir::WalkDir;

/// Decrypt RPG Maker MV Resources
#[derive(FromArgs)]
struct Args {
    /// the base directory of the game data, i.e. /www.
    #[argh(positional)]
    base_dir: PathBuf,

    /// the location to store the decrypted files
    #[argh(positional)]
    target: PathBuf,
}

fn system_data(base: impl AsRef<Path>) -> io::Result<SystemData> {
    let path = base.as_ref().join("data").join("System.json");
    let file = BufReader::new(File::open(path)?);
    let res = serde_json::from_reader(file)?;
    Ok(res)
}

fn main() -> io::Result<()> {
    let args: Args = argh::from_env();

    let system = system_data(&args.base_dir)?;

    for entry in WalkDir::new(&args.base_dir)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.file_type().is_file())
    {
        let src_path = entry.path();
        if let Some(ext) = decrypt_ext(&src_path) {
            let rel_path = src_path.strip_prefix(&args.base_dir).unwrap();
            let dst_path = args.target.join(rel_path).with_extension(ext);

            let src_file = BufReader::new(File::open(src_path)?);
            let dst_file = BufWriter::new(create_all(dst_path)?);
            decrypt(&system.encryption_key, src_file, dst_file)?;
        }
    }

    Ok(())
}

fn create_all(path: impl AsRef<Path>) -> io::Result<File> {
    if let Some(parent) = path.as_ref().parent() {
        std::fs::create_dir_all(parent)?;
    }

    File::create(path)
}

fn decrypt_ext(path: impl AsRef<Path>) -> Option<&'static str> {
    match path.as_ref().extension().and_then(OsStr::to_str) {
        Some("rpgmvo") => Some("ogg"),
        Some("rpgmvm") => Some("m4a"),
        Some("rpgmvp") => Some("png"),
        _ => None,
    }
}

fn decrypt(key: &[u8], mut source: impl Read, mut target: impl Write) -> io::Result<()> {
    let header_len = key.len();
    let mut buf = vec![0; header_len];

    // skip the header
    source.read_exact(&mut buf)?;

    // decrypt the first section
    source.read_exact(&mut buf)?;
    rpg_mv::decrypt(key, &mut buf);
    target.write_all(&buf)?;

    // the rest isn't even encrypted
    io::copy(&mut source, &mut target)?;
    Ok(())
}
